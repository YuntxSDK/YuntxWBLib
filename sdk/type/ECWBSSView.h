//
//  wbssView.h
//  Hello_Triangle
//
//  Created by Sean Lee on 16/6/23.
//  Copyright © 2016年 Daniel Ginsburg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>
#import <OpenGLES/EAGL.h>

/**
 * 白板显示类
 */
@interface ECWBSSView : UIView

/**
 @brief 白板是否可编辑
 */
@property (nonatomic, assign) BOOL isEdit;

/**
 isAddTextModel 是否是添加文字模式
 */
@property (nonatomic, assign) BOOL isTextAddModel;

/**
 isDeleteModel 是否是删除划线模式
 */
@property (nonatomic, assign) BOOL isDeleteModel;


- (void)toucheWithPointInAddTextModel:(CGPoint)point;

- (void)hiddenTextAddingView;

- (void)showAddTextSettingView;

- (void)showAddTextSettingView:(UIColor *)textColor withFont:(int)font fontPath:(NSString *)fontPath;

- (void)hiddenAddTextSettingView;

- (void)changeTextViewFont;

- (void)changeTextViewColor;

@end
