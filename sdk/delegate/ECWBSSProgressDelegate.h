//
//  ECProgressDelegate.h
//  CCPiPhoneSDK
//
//  Created by jiazy on 14/11/7.
//  Copyright (c) 2014年 yuntongxun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ECWBSSDocument.h"

/**
 * 文档上传进度代理
 */
@protocol ECWBSSProgressDelegate <NSObject>

/**
 @brief 上传进度
 @discussion 用户需实现此接口用以支持进度显示
 @param upLoadSize 已上传大小
 @param upLoadSize 总大小
 @param document  某一文档
 */
- (void)setUploadSize:(long long)upLoadSize totalSize:(long long)totalSize forDocument:(ECWBSSDocument *)document;

/**
 @brief 转换进度
 @discussion 用户需实现此接口用以支持进度显示
 @param progress 值域为0到1.0的浮点数
 @param document  某一文档
 */
- (void)setDocConvertProgress:(float)progress forDocument:(ECWBSSDocument *)document;

/**
 @brief 下载进度
 @discussion 用户需实现此接口用以支持进度显示
 @param progress 值域为0到1.0的浮点数
 @param document  某一文档
 */
- (void)setDownloadProgress:(float)progress forDocument:(ECWBSSDocument *)document;

@required

@end
