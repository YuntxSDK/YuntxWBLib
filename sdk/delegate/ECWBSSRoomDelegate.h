//
//  ECWBSSRoomDelegate.h
//  WBSSiPhoneSDK
//
//  Created by jiazy on 16/6/20.
//  Copyright © 2016年 yuntongxun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ECWBSSRoom.h"
#import "ECWBSSRoomManager.h"
#import "ECWBSSError.h"

/**
 * 房间通知代理
 */
@protocol ECWBSSRoomDelegate <NSObject>

/*
 @param userId 用户ID
 @param auth 用户权限
 @param type 类型 0 代表收回权限 1 代表赋予权限
 */
- (void) onReceivemNotifyChangeMember:(NSString*)userId auth:(MemberAuth)auth andType:(int)type;

/*
 @param roomId 房间ID
 @param userId 用户ID
 @param 房间解散通知
 */

- (void) onDeleteRoomNotifyRoomId:(int)roomId UserId:(NSString*)userId;

/*
 @param userId 用户ID
 @param roomId 房间ID
 @param 同一用户登陆被踢通知
 */

- (void) onKickOffNotifyRoomId:(int)roomId UserId:(NSString*)userId;

/*
 @param roomId 房间ID
 @param userId 用户ID
 @param 房间资源清除通知
 */

- (void) onClearRoomNotifyRoomId:(int)roomId UserId:(NSString*)userId;

/*
 @param roomId 房间ID
 @param userId 用户ID
 @param 用户加入房间通知
 */

- (void) onMemberJoinNotifyRoomId:(int)roomId UserId:(NSString*)userId;

/*
 @param roomId 房间ID
 @param userId 用户ID
 @param 用户离开房间通知
 */

- (void) onMemberLeaveNotifyRoomId:(int)roomId UserId:(NSString*)userId;

/*
 * @断网重连 刷新房间信息,数据通知
 * document
 *
 **/
-(void)onGetSyncRoomDataNotify:(ECWBSSRoom*)room;

/**
 @brief 绘制区域viewport的位置回调函数（绘制内容在ECWBSSView的内容视图glkview上显示的位置）
 */
-(void)onViewPortNotify:(CGRect)frame;


@end
