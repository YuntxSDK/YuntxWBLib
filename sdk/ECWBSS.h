//
//  ECWBSS.h
//  ECWBSS
//
//  Created by jiazy on 16/6/20.
//  Copyright © 2016年 yuntongxun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ECWBSSManager.h"
#import "ECWBSSDelegate.h"

/**
 * 白板和文档共享类 使用该类的单例操作
 */
@interface ECWBSS : NSObject

/**
 @brief 单例
 @discussion 获取该类单例进行操作
 @return 返回类实例
 */
+(ECWBSS*)sharedInstance;

/**
 @brief 设置应用信息
 @discussion 调用WBSSManger操作前，需要先设置应用信息和账号
 @param appId 应用ID
 @param auth 应用auth
 @param userId 用户ID
 */
-(void)setWBSSAppId:(NSString*)appId andAuth:(NSString*)auth andUserId:(NSString*)userId;

/**
 @brief 设置字体资源路径
 @param path 资源地址
 */
- (void)setDefaultResourceRootPath:(NSString *)path;

/**
 @brief 设置应用地址
 @param server 服务器地址
 */
-(void)setServerJson:(NSString*)server;


/**
 @brief 是否开启HTTPS
 @param enabel YES:开启HTTPS； NO:关闭HTTPS，默认为NO
 */
-(void)setEnabelHTTPS:(BOOL)enabel;

/**
 @brief SDK业务超时设置 如果不设置，底层会按照默认的超时设置
 @param reptimeout 服务器返回超时
 @param notifyTimeOut 上传文件之后转换通知超时
 */
-(void)setNetTimeOut:(int)reptimeout notifyTimeOut:(int)notifyTimeOut;


/**
 @brief 设置是否开启白板日志

 @param enable 是否开启 YES/NO
 @param path 日志保存路径 ，传NULL 则内部自动创建路径
 @param level 日志级别 0:不打印日志；1:只开启OC日志 ;  2:开启 C++底层日志 3:全部日志 。默认为0,不打印日志
 */
- (void)setWBSSTraceFlag:(BOOL)enable logPath:(NSString *)path logLevel:(int)level;

/**
 @brief ECWBSS代理
 @discussion 用于监听通知事件
 */
@property (nonatomic, weak) id<ECWBSSDelegate> delegate;

/**
 @brief 操作类
 @discussion 用于房间管理和文档管理
 */
@property (nonatomic, readonly, strong) id<ECWBSSManager> WBSSManger;
@end
